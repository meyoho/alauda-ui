FROM nginx:alpine

WORKDIR /alauda-ui/
COPY dist /alauda-ui/
COPY nginx.conf /alauda-ui/

EXPOSE 9002
CMD ["nginx", "-c", "/alauda-ui/nginx.conf"]
