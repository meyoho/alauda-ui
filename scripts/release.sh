#!/bin/sh

apk add git curl jq

ACCESS_TOKEN=$(curl -s -X POST -u "$CLIENT_ID:$CLIENT_SECRET" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=client_credentials -d scopes="repository"| jq --raw-output '.access_token')

git remote set-url origin "https://x-token-auth:$ACCESS_TOKEN@bitbucket.org/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG"

PUBLISH_VERSION=$(node scripts/publish-version)
PUBLISH_BRANCH=$(node scripts/publish-branch)
NPM_TAG=$(node scripts/npm-tag)

npm set //registry.npmjs.org/:_authToken "$NPM_TOKEN"

if [ "$NPM_TAG" = "beta" ]
then
  SRC_PKG=package.json
  TMP=$(mktemp)
  jq ".version = \"$PUBLISH_VERSION\"" "$SRC_PKG" > "$TMP"
  mv -f "$TMP" "$SRC_PKG"
else
  git checkout -B "$PUBLISH_BRANCH" origin/"$PUBLISH_BRANCH"
  yarn release --release-as "$PUBLISH_VERSION"
fi

yarn build

# CHANGELOG should only be pushed on branch master
if [ "$(git branch | grep '\*' | cut -d ' ' -f2)" = "master" ] || [ "$PUBLISH_BRANCH" != "master" ]
then
  git push --follow-tags origin "$PUBLISH_BRANCH"
elif [ "$NPM_TAG" != "beta" ]
then
  RED=$(printf '\033[31m')
  RESET=$(printf '\033[m')
  echo "${RED}You're trying to publish a latest version but not in the master branch, this is forbidden, maybe you'll want to use beta version.${RESET}"
  exit 1
fi

npm publish release --tag "$NPM_TAG"
